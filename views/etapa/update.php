<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\etapa $model */

$this->title = 'Update Etapa: ' . $model->numetapa;
$this->params['breadcrumbs'][] = ['label' => 'Etapas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->numetapa, 'url' => ['view', 'numetapa' => $model->numetapa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="etapa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
