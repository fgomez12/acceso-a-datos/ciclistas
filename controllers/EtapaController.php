<?php

namespace app\controllers;

use app\models\etapa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EtapaController implements the CRUD actions for etapa model.
 */
class EtapaController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all etapa models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => etapa::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'numetapa' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single etapa model.
     * @param int $numetapa Numetapa
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($numetapa)
    {
        return $this->render('view', [
            'model' => $this->findModel($numetapa),
        ]);
    }

    /**
     * Creates a new etapa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new etapa();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'numetapa' => $model->numetapa]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing etapa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $numetapa Numetapa
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($numetapa)
    {
        $model = $this->findModel($numetapa);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'numetapa' => $model->numetapa]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing etapa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $numetapa Numetapa
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($numetapa)
    {
        $this->findModel($numetapa)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the etapa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $numetapa Numetapa
     * @return etapa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($numetapa)
    {
        if (($model = etapa::findOne(['numetapa' => $numetapa])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
